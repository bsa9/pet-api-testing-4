import { expect } from 'chai';
import { AuthController } from '../lib/controllers/auth.controller';
import { ArticlesController } from '../lib/controllers/articles.controller';
import { UserController } from '../lib/controllers/user.controller';
import { AuthorController } from '../lib/controllers/author.controller';

import {
    checkStatusCode,
    checkResponseBodyStatus,
    checkResponseBodyMessage,
    checkResponseTime,
    checkSchema
} from '../../helpers/functionsForChecking.helper';

const schemas = require('./data/schemas.json');
var chai = require('chai');
chai.use(require('chai-json-schema'));

const auth = new AuthController();
const articles = new ArticlesController();
const user = new UserController();
const author = new AuthorController();

describe('Author auto tests', () => {
    let accessToken: string, userId: string, authorId: string, articleId: string;
	let authorSettings: any, responseSettings: any;
    let articlesCounter: number;

    let invalidAuthorCredentialsDataSet = [
        { email: 'petro.vaskovskyi@yahoo.com', password: '      ' },
        { email: 'petro.vaskovskyi@yahoo.com', password: '' },
        { email: 'petro.vaskovskyi@yahoo.com', password: '111' },
        { email: 'petro.vaskovskyi@yahoo.com', password: 'qwerty 123456' },
        { email: 'petro.vaskovskyi@yahoo.com', password: 'admin' },
        { email: 'petro.vaskovskyi@yahoo.com', password: 'petro.vaskovskyi@yahoo.com' },
        { email: 'petro.vaskovskyi @yahoo.com', password: 'qwerty123456' },
        { email: 'petro.vaskovskyi@yahoo.com   ', password: 'qwerty123456' },
    ];

    let invalidCommentsSizecode400 = [
        { sizeValue: 111111111111111111111111111111111111111111111111111111111111111111 },        
        { sizeValue: -111111111111111111111111111111111111111111111111111111111111111111 },                
        { sizeValue: 0.1 },
        { sizeValue: -1.2345345345 },                
    ];

    let invalidCommentsSizecode500 = [
        { sizeValue: 0 },
        { sizeValue: -1 },
    ];

    before(`should get access token and userId`, async () => {
        let response = await auth.authenticateUser(global.appConfig.users.author.email, global.appConfig.users.author.password);
        checkStatusCode(response, 200);
        accessToken = response.body.accessToken;
    
        response = await user.getCurrentUser(accessToken);
        checkStatusCode(response, 200);
        authorId = response.body.id;
    });

    it(`Author Login`, async () => {
        let response = await auth.authenticateUser(global.appConfig.users.author.email, global.appConfig.users.author.password);
        accessToken = response.body.accessToken;
        checkStatusCode(response, 200);
        checkResponseTime(response);
        checkSchema(response, schemas.schema_login_response);
    });

    it(`Get author settings`, async () => {
        let response = await author.getSettings(accessToken);
        responseSettings = response;
        authorSettings = response.body;
        authorId = response.body.id;
        checkStatusCode(response, 200);
        checkResponseTime(response);
        checkSchema(response, schemas.schema_get_settings_author);
    });

    it(`Update author settings`, async () => {
        authorSettings.job = 'Updated BSA Author';
        authorSettings.company = 'Updated BSA';
        authorSettings.location = 'Poland';
		
        await author.setSettings(accessToken, authorSettings);
        checkStatusCode(responseSettings, 200);
        checkResponseTime(responseSettings);
        checkSchema(responseSettings, schemas.schema_get_settings_author);
    });	

    it(`Get user details`, async () => {
        let response = await user.getCurrentUser(accessToken);
        checkStatusCode(response, 200);
        checkResponseTime(response);
        checkSchema(response, schemas.schema_getcurrentuser);
    });

    it(`Get Public Author`, async () => {
        let response = await author.getPublicAuthor(accessToken, authorId);
        checkStatusCode(response, 200);
        checkResponseTime(response);
        checkSchema(response, schemas.schema_getpublicauthor);
        console.log(response.body);
    });

    it(`Get articles`, async () => {
        let response = await articles.getArticles(accessToken);
        articlesCounter = response.body.length;
        checkStatusCode(response, 200);
        checkResponseTime(response);
        checkSchema(response, schemas.schema_get_articles);
    });

    it(`Add article`, async () => {
        let newArticle = {
            name: 'Test article',
            image: 'https://knewless.tk/assets/images/35dcffdf-ed92-4d94-aa32-67ea0f8afe39.JPG',
            text: '<p>Text of test article</p>\n',
            uploadImage: null
        };

        let response = await articles.saveArticle(accessToken, newArticle);
        articlesCounter += 1;
        articleId = response.body.id;
        checkStatusCode(response, 200);
        checkResponseTime(response);
        checkSchema(response, schemas.schema_post_article);
    });

    it(`Get articles after adding new one`, async () => {
        let response = await articles.getArticles(accessToken);
        checkStatusCode(response, 200);
        checkResponseTime(response);
        checkSchema(response, schemas.schema_get_articles);
        expect(response.body.length).to.be.equal(articlesCounter);
    });

    it(`Get article by ID`, async () => {
        let response = await articles.getArticleById(accessToken, articleId);
        checkStatusCode(response, 200);
        checkResponseTime(response);
        checkSchema(response, schemas.schema_article_by_id);
    });

    it(`Add comment to article`, async () => {
        let response = await articles.addComment(accessToken, articleId, 'Some text in test comment');
        checkStatusCode(response, 200);
        checkResponseTime(response);
        checkSchema(response, schemas.schema_add_comment_to_article);
    });
    
    it(`Get comments from article by pieces (limited by 200 items)`, async () => {
        let response = await articles.getArticleComment(accessToken, articleId, 200);
        checkStatusCode(response, 200);
        checkResponseTime(response);
        checkSchema(response, schemas.schema_article_comments);
    });

    //negative tests
	
    invalidAuthorCredentialsDataSet.forEach((credentials) => {
        it(`Negative test - Login using invalid credentials : ${credentials.email} + ${credentials.password}`, async () => {
            let response = await auth.authenticateUser(credentials.email, credentials.password);

            checkStatusCode(response, 401);
            checkResponseBodyStatus(response, 'UNAUTHORIZED');
            checkResponseBodyMessage(response, 'Bad credentials');
            checkResponseTime(response, 3000);
        });
    });

    invalidCommentsSizecode400.forEach((size) => {
        it(`Negative test - Read commnents from article with size : ${size.sizeValue}`, async () => {
            let response = await articles.getArticleComment(accessToken, articleId, size.sizeValue)

            checkStatusCode(response, 400);
            checkResponseTime(response, 3000);
        });
    });    

    invalidCommentsSizecode500.forEach((size) => {
        it(`Negative test - Read commnents from article with size : ${size.sizeValue}`, async () => {
            let response = await articles.getArticleComment(accessToken, articleId, size.sizeValue)

            checkStatusCode(response, 500);
            checkResponseBodyStatus(response, 'INTERNAL_SERVER_ERROR');
            checkResponseBodyMessage(response, 'Unexpected error');
            checkResponseTime(response, 3000);
        });
    });  

     afterEach(function () {
        console.log('for author tests');
    })

});
