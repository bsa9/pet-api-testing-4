import { expect } from "chai";
import { CoursesController } from '../lib/controllers/courses.controller';
import { checkResponseTime, checkStatusCode, checkSchema } from '../../helpers/functionsForChecking.helper';

const courses = new CoursesController();

const schemas = require('./data/schemas.json');
var chai = require('chai');
chai.use(require('chai-json-schema'));

describe("Courses controller", () => {
    let courseId: string;

    it(`getAllCourses`, async () => {
        let response = await courses.getAllCourses();
        courseId = response.body[9].id;
        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        expect(response.body.length).to.be.greaterThan(1);
    });

    it(`getPopularCourses`, async () => {
        let response = await courses.getPopularCourses();
        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        expect(response.body.length).to.be.greaterThan(1);
    });
    
    it(`getCourseDetails`, async () => {
        let response = await courses.getCourseInfoById(courseId);
        checkStatusCode(response, 200);
        checkResponseTime(response, 3000); 
        checkSchema(response, schemas.schema_courseInfo); 
    });
});
