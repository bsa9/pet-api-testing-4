import { expect } from 'chai';

import {
    checkResponseTime,
    checkStatusCode,
    checkResponseBodyStatus,
    checkResponseBodyMessage,
    checkSchema
} from '../../helpers/functionsForChecking.helper';

import { AuthController } from '../lib/controllers/auth.controller';
import { UserController } from '../lib/controllers/user.controller';
import { StudentController } from '../lib/controllers/student.controller';
import { FavoritesController } from '../lib/controllers/favorites.controller';

const schemas = require('./data/schemas.json');
var chai = require('chai');
chai.use(require('chai-json-schema'));

const auth = new AuthController();
const user = new UserController();
const student = new StudentController();
const favorites = new FavoritesController();

describe('Student auto tests', () => {

    let accessToken: string, studentId: string, goalId: string;
	let userInfo: any, studentSettings: any, studentGoal: any, studentData: any, studentProfile: any, favoriteCourses: any;
   
   let invalidCredentialsDataSet = [
        { email: 'petro.getman@i.ua', password: ' ' },
        { email: 'petro.getman@i.ua', password: 'jdurdPAss23fskwe ' },
        { email: 'petro.getman@i.ua', password: '  jdurdPAss23fskwe' },
        { email: 'petro.getman@i.ua', password: 'jdurdPA ss23fskwe' },
        { email: 'petro.getman@i.ua', password: 'admin' },
        { email: 'petro.getman@i.ua', password: 'petro.getman@i.ua' },
        { email: 'petro.getman @i.ua ', password: 'jdurdPAss23fskwe' },
        { email: ' ', password: 'jdurdPAss23fskwe' },
        { email: 'petro@getman@i.', password: 'jdurdPAss23fskwe' },
        { email: 'petro.getman@i.ua  ', password: 'jdurdPAss23fskwe' },
    ];

    before(`should get access token and userId`, async () => {
        let response = await auth.authenticateUser(global.appConfig.users.student.email, global.appConfig.users.student.password);
        checkStatusCode(response, 200);
        accessToken = response.body.accessToken;

        response = await user.getCurrentUser(accessToken);
        checkStatusCode(response, 200);
        studentId = response.body.id;
    });


    it(`Student Login`, async () => {
        let response = await auth.authenticateUser(global.appConfig.users.student.email, global.appConfig.users.student.password);
        accessToken = response.body.accessToken;
        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkSchema(response, schemas.schema_login_response);
    });

    it(`Get user info`, async () => {
        let response = await user.getCurrentUser(accessToken);
        userInfo = response.body;
        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkSchema(response, schemas.schema_userInfo);
    });
	
    it(`Get Student settings`, async () => {
        let response = await student.getSettings(accessToken);
        studentSettings = response.body;
        studentId = response.body.id;
        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkSchema(response, schemas.schema_studentSettings);
    });
	
    it(`Update Student settings`, async () => {
		
        studentSettings.job = 'Updated BSA Student';
        studentSettings.company = 'Updated BSA';
        studentSettings.location = 'Poland';
        studentSettings.website = 'abc.com';

        let response = await student.setSettings(accessToken, studentSettings);
        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkSchema(response, schemas.schema_update_settings);        
    });

    it(`Get Student settings`, async () => {
        let response = await student.getSettings(accessToken);
        studentSettings = response.body;
        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkSchema(response, schemas.schema_studentSettings);
        console.log('Student settings updated');
    });	
    
    it(`Get goals`, async () => {
        let response = await student.getGoals(accessToken);
        goalId = response.body[0].id;
        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkSchema(response, schemas.schema_goals);
    });

    it(`Set Student goal`, async () => {
        let response = await student.setGoal(accessToken, goalId);
        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
    });
    

    it(`Get Student progress (goal)`, async () => {
        let response = await student.getCurrentProgress(accessToken);
        studentGoal = response.body;
        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkSchema(response, schemas.schema_student_goal);
    });	
		
    it(`Get Student data`, async () => {
        let response = await student.getStudentData(accessToken);
        studentData = response.body;
        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkSchema(response, schemas.schema_student_data);
        expect(response.body.id).to.be.equal(studentId);
    });

    it(`Get Student profile`, async () => {
        let response = await student.getStudentProfile(accessToken);
        studentProfile = response.body;
        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkSchema(response, schemas.schema_student_profile);
    });

    it(`Get Favorite Courses`, async () => {
        let response = await favorites.getFavoriteCourses(accessToken);
        favoriteCourses = response.body;
        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkSchema(response, schemas.schema_favorite_courses);
    });
	
	//negative

    invalidCredentialsDataSet.forEach((credentials) => {
        it(`Negative test - login using invalid credentials : ${credentials.email} + ${credentials.password}`, async () => {
            let response = await auth.authenticateUser(credentials.email, credentials.password);

            checkStatusCode(response, 401);
            checkResponseBodyStatus(response, 'UNAUTHORIZED');
            checkResponseBodyMessage(response, 'Bad credentials');
            checkResponseTime(response, 3000);
        });
    });

    afterEach(function () {
        console.log('for student tests');
    });
    
});
