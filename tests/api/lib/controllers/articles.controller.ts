import { ApiRequest } from '../request';

let baseUrl: string = global.appConfig.baseUrl;

export class ArticlesController {
    
	async getArticles(accessToken: string) {
        const response = await new ApiRequest()
			.prefixUrl(baseUrl)
            .method('GET')
            .url(`article/author`)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async saveArticle(accessToken: string, articleObj: object) {
        const response = await new ApiRequest()
			.prefixUrl(baseUrl)
            .method('POST')
            .url(`article`)
            .bearerToken(accessToken)
            .body(articleObj)
            .send();
        return response;
    }

    async addComment(accessToken: string, articleIdValue: string, textValue: string) {
        const response = await new ApiRequest()
			.prefixUrl(baseUrl)
            .method('POST')
            .url(`article_comment`)
            .bearerToken(accessToken)
            .body({
                articleId: articleIdValue,
                text: textValue,
            })
            .send();
        return response;
    }

	async getArticleById(accessToken: string, articleIdValue: string) {
        const response = await new ApiRequest()
			.prefixUrl(baseUrl)
            .method('GET')
            .url(`article/${articleIdValue}`)
            .bearerToken(accessToken)
            .send();
        return response;
    }	

	async getArticleComment(accessToken: string, articleIdValue: string, sizeValue: number) {
        const response = await new ApiRequest()
			.prefixUrl(baseUrl)
            .method('GET')
            .url(`article_comment/of/${articleIdValue}?size=${sizeValue}`)
            .bearerToken(accessToken)
            .send();
        return response;
    }	
	
}
