import { ApiRequest } from '../request';

let baseUrl: string = global.appConfig.baseUrl;

export class AuthController {
	
    async authenticateUser(emailVal: string, passwordVal: string) {
        const response = await new ApiRequest()
			.prefixUrl(baseUrl)
            .method('POST')
            .url(`auth/login`)
            .body({
                email: emailVal,
                password: passwordVal,
            })
            .send();
        return response;
    }
	
}
