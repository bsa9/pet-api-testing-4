import { expect } from 'chai';

export function checkStatusCode(response, statusCode: 200 | 201 | 204 | 400 | 401 | 403 | 404 | 409 | 500) {
    expect(response.statusCode, `Status Code should be ${statusCode}`).to.equal(statusCode);
   
}

export function checkResponseBodyStatus(response, status: string) {
    expect(response.body.status, `Status should be ${status}`).to.be.equal(status);
}

export function checkResponseBodyMessage(response, message: string) {
    expect(response.body.message, `Message should be ${message}`).to.be.equal(message);
}

export function checkResponseTime(response, maxResponseTime: number = 3000) {
    expect(response.timings.phases.total, `Response time should be less than ${maxResponseTime}ms`).to.be.lessThan(
        maxResponseTime
    );
}
export function checkSchema(response, schema: string) {
        expect(response.body, `Response body slould follow schema - ${schema}`).to.be.jsonSchema(schema);
    };
    
